# Installation et configuration de CentOS sur un MSI U130

Réglages pour le MSI U130 sous CentOS 7 avec environnement Xfce.

**[Voir le Wiki >>>](https://gitlab.com/sakura-lain/centos-msi/wikis/home)**

Voir aussi, pour le même ordinateur :

- [Debian](https://gitlab.com/sakura-lain/debian-msi)
- [FreeBSD](https://gitlab.com/sakura-lain/freebsd-msi)