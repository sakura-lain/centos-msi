# centos-msi

Réglages pour le MSI U130 sous CentOS 7 avec environnement Xfce.

Voir aussi, pour le même ordinateur :

- [Debian](https://gitlab.com/sakura-lain/debian-msi)
- [FreeBSD](https://gitlab.com/sakura-lain/freebsd-msi)

## Installer XFCE : 

- configurer le dépôt Epel : `yum install epel-release -y`
- installer Server with GUI : `yum groupinstall "Server with GUI" -y`
- installer XFCE : `yum groupinstall "Xfce" -y`

## Installer LightDM :

- configurer le dépôt Epel si ce n’est déjà fait puis : `yum install lightdm `
- vérifier l’existence et modifier si nécessaire la configuration du fichier `/etc/lightdm/lightdm.conf`. Le créer s’il n’existe pas.
- Désactiver GDM au profit de LightDM : `systemctl disable gdm ; systemctl enable lightdm`

[Netsarang](https://www.netsarang.com/forum/xmanager/4076/how_to_configure_xmanager_to_centos7)

## Supprimer l’image au démarrage :

- Dans Debian si c’est son grub qui est actif, dans CentOS sinon, éditer le fichier `/etc/default/grub` et modifier la ligne `GRUB_CMDLINE_LINUX_DEFAULT` en ajoutant `logo.nologo`  comme suit :

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash logo.nologo"
```

- Relancer `update-grub`

[Ubuntu-fr](https://doc.ubuntu-fr.org/grub-pc#exempleos_selectionne_par_defaut)

## Tableau de bord :

Ajouter des lanceurs au tableau de bord : à partir du le menu applications, glisser-déposer les icônes des programmes dans le tableau de bord.

## Prise en charge de NTFS :

Paquets Linux NTFS User Space Driver et NTFS Filesystem libraries and utilities

## Installer Clamav :

- si ce n'est déjà fait, installer le dépôt Epel : `yum install epel-release`
- installer tous les composants de Clamav :
	
```
yum install clamav-server clamav-data clamav-update clamav-filesystem clamav clamav-scanner-systemd clamav-devel clamav-lib clamav-server-systemd
```

- mettre à jour la base de données virales : freshclam

[Linux-audit](https://linux-audit.com/install-clamav-on-centos-7-using-freshclam/)

Attention à SELinux : 

[Hostpresto](https://hostpresto.com/community/tutorials/how-to-install-clamav-on-centos-7/)
[Wikipedia](https://fr.wikipedia.org/wiki/SELinux) 

## Installer VLC :

- En plus du dépôt Epel, installer le dépôt Nux-Desktop en copiant-collant cette ligne :

```	
rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
```

- installer VLC : yum install VLC

[Li.nux.ro](https://li.nux.ro/repos.html)
[Tecmint](https://www.tecmint.com/install-vlc-media-player-in-rhel-centos-fedora/)

## Installer un paquet isolé :

- se placer dans le répertoire contenant le paquet
- `rpm -ivh nom_du_paquet.rpm`

[Léa Linux](http://lea-linux.org/documentations/RPM)